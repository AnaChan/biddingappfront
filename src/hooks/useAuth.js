import { useState, useContext, createContext } from 'react';
const authContext = createContext();

const authentication = {
  isAuthenticated: false,
  login: (cb) => {
    authentication.isAuthenticated = true;
    cb();
  },
  logout(cb) {
    authentication.isAuthenticated = false;
    cb();
  },
};

function useProvideAuth() {
  const userJson = localStorage.getItem('user');
  const initUser = userJson ? JSON.parse(userJson) : null;
  const [user, setUser] = useState(initUser);

  const login = (userVal, cb) => {
    localStorage.setItem('user', JSON.stringify(userVal));
    return authentication.login(() => {
      setUser(userVal);
      cb();
    });
  };

  const logout = (cb) => {
    return authentication.logout(() => {
      setUser(null);
      cb();
    });
  };

  return {
    user,
    login,
    logout,
  };
}

export function ProvideAuth({ children }) {
  const auth = useProvideAuth();
  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

export function useAuth() {
  return useContext(authContext);
}
