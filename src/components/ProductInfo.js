import { Row, Col, Typography } from 'antd';
import Countdown from './Countdown';

const { Title, Text } = Typography;

function ProductInfo({
  isBidClosed,
  name,
  description,
  currentBid,
  closeDate,
}) {
  return (
    <>
      <Row>
        <Col span={10}>
          <Title>{name}</Title>
        </Col>
        <Col span={14}>
          <Countdown closeDate={closeDate} isBidClosed={isBidClosed} />
        </Col>
      </Row>
      <Row>
        <Title level={2}>${currentBid}</Title>
      </Row>
      <Row style={{ margin: '40px 0' }}>
        <Text type="secondary">{description}</Text>
      </Row>
    </>
  );
}

export default ProductInfo;
