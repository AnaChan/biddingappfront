import { Typography, Statistic } from 'antd';
import { ClockCircleTwoTone, CheckCircleOutlined } from '@ant-design/icons';
const { Countdown: AntdCountdown } = Statistic;

const { Title } = Typography;

function Countdown({ isBidClosed, closeDate }) {
  const closed = (
    <>
      <CheckCircleOutlined />
      {closeDate.toLocaleDateString()}
    </>
  );

  const countdown = (
    <AntdCountdown
      valueStyle={{ fontSize: 38 }}
      prefix={<ClockCircleTwoTone />}
      value={closeDate}
      format="Dd:HH:mm:ss"
    />
  );

  return <Title>{isBidClosed ? closed : countdown}</Title>;
}

export default Countdown;
