import { Card, Button } from 'antd';
import { Link } from 'react-router-dom';
const { Meta } = Card;

function ProductListItem({ id, img, name, description }) {
  return (
    <Card cover={<img alt={name} src={img} />}>
      <Meta title={name} description={description} />
      <Button type="primary" size="large" style={{ marginTop: '16px' }}>
        <Link to={`/products/${id}`}> Bid Now </Link>
      </Button>
    </Card>
  );
}

export default ProductListItem;
