import { createServer, Model } from 'miragejs';

export function makeServer({ environment = 'test' } = {}) {
  let server = createServer({
    environment,

    models: {
      product: Model,
    },

    seeds(server) {},

    routes() {
      const hasAuth = (request) => {
        const { authorization: bearerToken } = request.requestHeaders;
        const token = bearerToken.split(' ');
        if (!token) return false;
        else return true;
      };

      this.namespace = 'api';

      this.get('/products', (schema, req) => {
        if (!hasAuth(req)) return new Response(401);
        const data = [
          {
            id: 1,
            name: 'Courtney Gleichner',
            description:
              'Incidunt amet sunt voluptatem eius vel cum deserunt. Aperiam corporis ratione in quaerat et accusamus. Aut modi omnis blanditiis est. At quia facere voluptatem quo aliquam omnis. Dignissimos provident incidunt maxime corporis optio at. Eos laboriosam sint dolor. Laudantium velit ut vel voluptas.',
            img: 'https://icon-library.com/images/image-icon/image-icon-16.jpg',
            init_bid_amount: 25,
            end_bid_date: null,
            end_bid_time: null,
          },
          {
            id: 2,
            name: 'Alexa Collier',
            description:
              'Dolorem et autem repudiandae cumque quos reiciendis. Distinctio sed eum velit consectetur. Placeat et ea aut inventore architecto ad. Impedit consequatur consequatur incidunt qui fuga. Velit aliquid magni rerum aperiam odio. Rem velit qui quam. Voluptas aut harum est et officiis velit.',
            img: 'https://icon-library.com/images/image-icon/image-icon-16.jpg',
            init_bid_amount: 10,
            end_bid_date: null,
            end_bid_time: null,
          },
          {
            id: 3,
            name: 'Susan Windler',
            description:
              'Blanditiis voluptatibus ducimus enim sit in. Ut illo illo id non. Non sint quasi libero veniam quisquam dolores iste ad. Sit vel eveniet autem cumque error ipsum voluptas. Facere et qui aut eum id dolores ea alias. Non eos voluptates eos sequi blanditiis eius.',
            img: 'https://icon-library.com/images/image-icon/image-icon-16.jpg',
            init_bid_amount: 5,
            end_bid_date: null,
            end_bid_time: null,
          },
          {
            id: 4,
            name: 'Myles Huels',
            description:
              'Veritatis ducimus officia quia ab ratione. Eos aspernatur omnis et sit qui eveniet. Voluptatum nam officiis sed impedit. Libero ut laboriosam est sint illo cum eius id. Natus enim est eos. Est doloribus eos qui doloribus. Sit sunt maiores ut autem. Et eos fugiat nobis et.',
            img: 'https://icon-library.com/images/image-icon/image-icon-16.jpg',
            init_bid_amount: 19,
            end_bid_date: null,
            end_bid_time: null,
          },
          {
            id: 5,
            name: 'Winona Mertz',
            description:
              'Et laboriosam numquam maiores et cumque recusandae molestiae. Doloremque modi vel corrupti quae. Vero eum quisquam voluptas qui tenetur. Eaque reiciendis et tempore est. Distinctio quia nulla totam. Quia dolores qui quisquam. Reprehenderit sapiente harum blanditiis velit accusantium quis totam.',
            img: 'https://icon-library.com/images/image-icon/image-icon-16.jpg',
            init_bid_amount: 23,
            end_bid_date: null,
            end_bid_time: null,
          },
          {
            id: 6,
            name: 'Hipolito Hane',
            description:
              'Velit eligendi doloremque ipsam enim. Earum nisi temporibus consectetur veniam ut amet. Et aut modi at delectus. Cupiditate nulla quo facilis sed accusantium ullam accusantium.',
            img: 'https://icon-library.com/images/image-icon/image-icon-16.jpg',
            init_bid_amount: 19,
            end_bid_date: null,
            end_bid_time: null,
          },
          {
            id: 7,
            name: 'Liliane Friesen DVM',
            description:
              'Exercitationem maxime vel eius hic sit. Et placeat ipsa harum et quos laboriosam et laborum. Vel commodi nihil optio explicabo voluptas aut quod. Qui voluptatibus veritatis doloremque enim et. Deserunt sed explicabo officia maiores pariatur. Minus autem molestiae nemo labore.',
            img: 'https://icon-library.com/images/image-icon/image-icon-16.jpg',
            init_bid_amount: 8,
            end_bid_date: null,
            end_bid_time: null,
          },
          {
            id: 8,
            name: 'Providenci Will',
            description:
              'Animi magni quos veritatis voluptatem esse ab incidunt. Pariatur nihil eos voluptatem soluta rerum. Ullam odit at perspiciatis voluptatem aut consequatur libero odit. Saepe rerum neque sunt mollitia qui vitae quis.',
            img: 'https://icon-library.com/images/image-icon/image-icon-16.jpg',
            init_bid_amount: 12,
            end_bid_date: null,
            end_bid_time: null,
          },
          {
            id: 9,
            name: 'Orie Lockman',
            description:
              'Quia omnis molestiae in quis omnis quo eos optio. Est cum dicta dolorem quaerat. Aliquid expedita voluptatem odio ut sunt. Doloremque aut cupiditate aspernatur nesciunt. Eos labore sunt non. Optio iste et ea debitis est.',
            img: 'https://icon-library.com/images/image-icon/image-icon-16.jpg',
            init_bid_amount: 15,
            end_bid_date: null,
            end_bid_time: null,
          },
          {
            id: 10,
            name: 'Dr. Pansy Graham',
            description:
              'Et reprehenderit saepe ad ut nam sapiente et. Eaque dolor dolor asperiores voluptatem temporibus porro pariatur. Minus ex nesciunt quis eaque aut est. Est esse veritatis voluptas sint explicabo officia.',
            img: 'https://icon-library.com/images/image-icon/image-icon-16.jpg',
            init_bid_amount: 8,
            end_bid_date: null,
            end_bid_time: null,
          },
        ];
        const secondPage = data.map((p) => ({ ...p, name: p.name + 'page 2' }));
        const thirdPage = data
          .slice(0, 4)
          .map((p) => ({ ...p, name: p.name + 'page 3' }));
        const noDataPage = [];

        const getPageData = (page) => {
          switch (page) {
            case '1':
              return data;
            case '2':
              return secondPage;
            case '3':
              return thirdPage;
            default:
              return noDataPage;
          }
        };

        const page = req.queryParams.page || 1;
        const pageData = getPageData(page);
        return {
          current_page: parseInt(page),
          data: pageData,
          first_page_url: 'http://127.0.0.1:8000/api/products?page=1',
          from: 1,
          last_page: 3,
          last_page_url: 'http://127.0.0.1:8000/api/products?page=3',
          next_page_url: 'http://127.0.0.1:8000/api/products?page=2',
          path: 'http://127.0.0.1:8000/api/products',
          per_page: 10,
          prev_page_url: null,
          to: 10,
          total: 30,
        };
      });

      this.get('/products/:id', (schema, req) => {
        if (!hasAuth(req)) return new Response(401);
        const id = parseInt(req.params.id);
        const product = {
          id,
          name: `Courtney Gleichner ${id}`,
          description:
            'Incidunt amet sunt voluptatem eius vel cum deserunt. Aperiam corporis ratione in quaerat et accusamus. Aut modi omnis blanditiis est. At quia facere voluptatem quo aliquam omnis. Dignissimos provident incidunt maxime corporis optio at. Eos laboriosam sint dolor. Laudantium velit ut vel voluptas.',
          img: 'https://icon-library.com/images/image-icon/image-icon-16.jpg',
          init_bid_amount: 25,
          end_bid_date: new Date('2021-06-20T06:00:00Z'),
        };
        return product;
      });

      this.post('/login', (schema, request) => {
        const { email } = request.requestBody;
        return {
          token: 'OhRYvoCucLA7jXsmyufK4IC3t3MMo75uzMC5VWK2qyjPb148wjDX4WoZfNsS',
          email,
        };
      });

      this.post('/logout', (schema, request) => {
        if (!hasAuth(request)) return new Response(401);
        return new Response(200);
      });

      this.post('/bidXUsers', (schema, request) => {
        if (!hasAuth(request)) return new Response(401);
        const { userId, productId, bidAmount } = request.requestBody;
        return {
          userId,
          productId,
          bidAmount,
        };
      });
    },
  });

  return server;
}
