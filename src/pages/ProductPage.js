import { useEffect, useState } from 'react';
import { Row, Col, Image, Typography, Switch, message } from 'antd';
import { useParams } from 'react-router-dom';

import BidAction from '../components/BidAction';
import ProductInfo from '../components/ProductInfo';
import { useAuth } from '../hooks/useAuth';

const { Text } = Typography;

function ProductPage() {
  const { id } = useParams();
  const [product, setProduct] = useState({
    max_bid: 0,
    end_bid_date: new Date(0),
  });
  const auth = useAuth();
  const { token } = auth.user;
  const userId = auth.user.id;

  useEffect(() => {
    fetch(`http://localhost:8000/api/products/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => res.json())
      .then((product) => {
        setProduct(product);
      });
  }, [id, token]);

  const bidAmount = product.max_bid;
  const minBid = product.max_bid + 1;
  const now = new Date();
  const closeDate = new Date(product.end_bid_date);
  const isBidClosed = now >= closeDate;
  const [newBidAmount, setNewBidAmount] = useState(bidAmount);

  const onBidChange = (value) => {
    setNewBidAmount(value);
  };
  const onEnableBiddingChange = (checked) => {
    fetch(`http://localhost:8000/api/bidXUsers`, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      method: 'PATCH',
      body: JSON.stringify({
        userId: userId,
        productId: id,
        enableDisableAB: checked,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        const msg = checked === true ? 'ENABLED' : 'DISABLED';
        message.success(`auto bidding was ${msg} successfully`);
      });
  };

  const onSubmitBid = () => {
    fetch(`http://localhost:8000/api/bidXUsers`, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({
        userId: userId,
        productId: id,
        bidAmount: newBidAmount,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        setProduct({ ...product, max_bid: newBidAmount });
        message.success(`bid for ${newBidAmount} was added successfully`);
      });

    //TODO get maxBid
  };

  return (
    <div>
      <Row gutter={48} style={{ margin: '32px 0px' }}>
        <Col span={12}>
          <Image preview={false} width='100%' src={product.img} />
        </Col>
        <Col span={12}>
          <ProductInfo
            {...product}
            currentBid={bidAmount}
            closeDate={closeDate}
            isBidClosed={isBidClosed}
          />
          <BidAction
            isBidClosed={isBidClosed}
            minBid={minBid}
            closingBid={minBid}
            onBidChange={onBidChange}
            onSubmitBid={onSubmitBid}
            bidAmount={newBidAmount}
          />
          {!isBidClosed && (
            <Row gutter={16} style={{ marginTop: '24px' }}>
              <Col>
                <Switch size='large' onChange={onEnableBiddingChange} />
              </Col>
              <Col>
                <Text>Enable auto-bidding</Text>
              </Col>
            </Row>
          )}
        </Col>
      </Row>
    </div>
  );
}

export default ProductPage;
