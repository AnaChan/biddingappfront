import { Form, Input, Button, Typography } from 'antd';
import { useHistory, useLocation } from 'react-router-dom';
import { useAuth } from '../hooks/useAuth';

const { Title } = Typography;

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 12,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

function LoginPage() {
  const history = useHistory();
  const location = useLocation();
  const auth = useAuth();

  const { from } = location.state || { from: { pathname: '/' } };

  const onFinish = (values) => {
    fetch(`http://localhost:8000/api/login`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(values),
    })
      .then((res) => res.json())
      .then((res) => {
        auth.login(res, () => {
          history.replace(from);
        });
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  const usernameField = (
    <Form.Item
      label='Email'
      name='email'
      rules={[
        {
          required: true,
          message: 'Please input your email!',
        },
      ]}
    >
      <Input />
    </Form.Item>
  );

  const passwordField = (
    <Form.Item
      label='Password'
      name='pws'
      rules={[
        {
          required: true,
          message: 'Please input your password!',
        },
      ]}
    >
      <Input.Password />
    </Form.Item>
  );
  return (
    <div>
      <Title style={{ textAlign: 'center', margin: '48px 0' }}>
        Bidding App
      </Title>
      <Form
        {...layout}
        name='basic'
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        {usernameField}
        {passwordField}

        <Form.Item {...tailLayout}>
          <Button type='primary' htmlType='submit'>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default LoginPage;
